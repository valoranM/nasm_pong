<h1 align="center">
    Pong
  <br>
  <img src="pong.png" alt="pong" width="512">
  <br>
</h1>

## 👾 simple pong in nasm assembler respecting the elf standard 👾


thanks to [IvyTheGreat01](https://github.com/IvyTheGreat01/tetris_x64_assembly) 
for his work on xlib function calls in nasm

# Get Pong

### Required
- nasm
- xlib (linux)

### Assembly
```sh
make
```

### Launching
```sh
make run
./pong
```

# Contribute

If you want to improve the code I invite you to make pull requests

# To do

- [X] Score display
- [X] Add a menu
- [X] improve collisions
- [X] Add middle separation
- [X] Add game over screen
- [X] Add a robot
