SRCS := $(wildcard src/*.asm)

pong: pong.o
	gcc -o pong pong.o -lX11 -no-pie

pong.o: ${SRCS}
	nasm -felf64 -g -o pong.o src/pong.asm

.PHONY = all clean

run: pong
	./pong

clean:
	rm *.o pong

