    ;X11 functions
    extern 	XOpenDisplay
	extern 	XDefaultScreen
	extern 	XBlackPixel
	extern	XWhitePixel
	extern 	XDefaultRootWindow
    extern  XCreateSimpleWindow
    extern  XSelectInput
    extern  XMapWindow
    extern  XDefaultGC
    extern  XAutoRepeatOff

    extern  XDefaultColormap
    extern  XAllocNamedColor
    extern  XSetForeground
    extern  XkbKeycodeToKeysym

    extern  XFillRectangle
    extern  XClearWindow
    extern  XDrawString
    
    extern  XCheckWindowEvent

    extern  XDestroyWindow
    extern  XCloseDisplay

    extern usleep

    extern putchar

    section .bss
display:    resb 8
screen:     resb 4
gc_black:   resb 4
gc_white:   resb 4
gc_def:     resb 8
parent:     resb 8
win:        resb 8
colormap:   resb 8
keysym:     resb 8
moove:      resb 8

xevent:    resb 192

xcols:
    struc xcol_struct
        .white: resb 16
        .black: resb 16
    endstruc

    section .data
    %include "src/data.asm"


    section .text
    global main
    %include "src/draw.asm"
    %include "src/setup.asm"
    %include "src/ball.asm"
    %include "src/key.asm"
    %include "src/pad.asm"
    %include "src/menu.asm"

main:
    call setup_xwindow

    .menu:
        call menu
        cmp rbx, 3
        je close
        call setup_data
    .prosEvent:
        call inputControl
        cmp rax, 1
        je close

    ;moove pad
    call moovePad

    ; doing ball collision
    call ball_collision

    ; ball moving
    mov eax, [ball.x_speed]
    mul dword [ball.x_dir]
    add [ball.x], eax

    mov eax, [ball.y_speed]
    mul dword [ball.y_dir]
    add [ball.y], eax

    mov ecx, 10
    cmp [j1.score], ecx
    je .menu
    cmp [j2.score], ecx
    je .menu

    .drawGame:
        call draw_game

    mov rdi, 5_000
    call usleep

    jmp .prosEvent

close:
    mov rdi, [display]
    mov rsi, [win]
    sub rsp, 8
    call XDestroyWindow
    mov rdi, [display]
    call XCloseDisplay
    add rsp, 8
    
    ; return 0
	mov	rax, 0
	ret
