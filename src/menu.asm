menu:
    .drawMenu:
    xor rbx, rbx
    .loopMenu:
        call set_color
        call draw_menu
        call keyMenu
        mov rdi, 5_000
        call usleep
        cmp rbx, 0
        je .loopMenu
    ret

keyMenuSwitch:  
    .check1:
        cmp ecx, [key1]
        jne .check2
        mov rbx, 1
        mov dword [nbPlayer], ebx
        jmp .end
    .check2:
        cmp ecx, [key2]
        jne .check3
        mov rbx, 2
        mov dword [nbPlayer], ebx
        jmp .end
    .check3:
        cmp ecx, [key3]
        jne .end
        mov rbx, 3
    .end:
        ret

keyMenu:
    mov rdi, [display]
    mov rsi, [win]
    mov rdx, [KeyReleaseMask]
    or rdx, [KeyPressMask]
    mov rcx, xevent
    sub rsp, 8
    call XCheckWindowEvent
    add rsp, 8

    cmp rax, 1
    jne .endProsEvent

    ;if (xevent.type == KeyPress)
    mov ecx, [xevent + 0]
    cmp ecx, [KeyPress]
    jne keyMenu             ;!= KeyPress

    mov rdi, [display]
    mov rsi, [xevent + 84]
    mov rdx, 0
    mov rcx, 0
    sub rsp, 8
    call XkbKeycodeToKeysym
    add rsp, 8
    mov [keysym], rax
    mov ecx, [keysym]
    
    call keyMenuSwitch

    .endProsEvent:
        ret