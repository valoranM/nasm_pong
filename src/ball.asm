reset_ball:
    mov dword [ball.x_speed], 1
    mov dword [ball.y_speed], 1
    mov dword [ball.x], 251
    mov dword [ball.y], 123
    ret

ball_collision:
    .check_up:
        cmp dword [ball.y], 0
        jg .check_down      ;if ball.y > 0
        neg dword [ball.y_dir]
        jmp .check_J1
    .check_down:
        mov eax, [ball.y]
        add eax, [ball.size]
        cmp eax, [height]
        jl .check_J1            ;if ball.y + ball.size < height
        neg dword [ball.y_dir]
    .check_J1:
        mov eax, [ball.x]
        mov ecx, [j1.x_pad]
        add ecx, [pad_width]
        cmp eax, ecx
        jne .check_J2           ; if ball.x != j1.x_pad
        mov ebx, [j1.y_pad]
        cmp ebx, [ball.y]
        jg .check_left          ; if ball.y > j1.y_pad

        add ebx, [pad_height]
        cmp ebx, [ball.y]
        jl .check_left          ; if ball.y > j1.y_pad + pad_height
        neg dword [ball.x_dir]
        jmp .end
    .check_J2:
        mov eax, [ball.x]
        cmp eax, [j2.x_pad]
        jne .check_left         ; if ball.x != j2.x_pad
        mov ebx, [j2.y_pad]
        cmp ebx, [ball.y]
        jg .check_left          ; if ball.y > j2.y_pad

        add ebx, [pad_height]
        cmp ebx, [ball.y]
        jl .check_left          ; if ball.y > j2.y_pad + pad_height
        neg dword [ball.x_dir]
        jmp .end
    .check_left:
        cmp dword [ball.x], 0
        jg .check_right     ;if ball.x > 0
        inc dword [j2.score]
        call reset_ball
        jmp .check_J1
    .check_right:
        mov eax, [ball.x]
        add eax, [ball.size]
        cmp eax, [width]
        jl .end             ;if ball.x + ball.size < width
        inc dword [j1.score]
        call reset_ball
    .end:
        ret