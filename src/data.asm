width:  dd 512
height: dd 256

last_time:  db 0

;stringDisplay
score_d:        dd 0
null:       db "Pong"
nullLen:    equ $-null
p1Win:      db "Player 1 Wins!"
p2Win:      db "Player 2 Wins!"
winLen:     equ 14
fitstLine:  db "1: Single player"
len1:       equ $-fitstLine
secondLine: db "2: MultiPlayer"
len2:       equ $-secondLine
thirdLine:  db "3: Exit"
len3:       equ $-thirdLine

;game data
pad_height:     dd 56
pad_width:      dd 10
pad_speed:      dd 1
nbPlayer:       dd 2
j1:
    .x_pad:     dd 0
    .y_pad:     dd 0
    .moove:     dd 0
    .score:     dd 0
j2:
    .x_pad:     dd 0
    .y_pad:     dd 0
    .moove:     dd 0
    .score:     dd 0
ball:
    .size:      dd 5 
    .x:         dd 0
    .y:         dd 0
    .x_dir:     dd 0
    .y_dir:     dd 0
    .x_speed:   dd 0
    .y_speed:   dd 0

; Xlib constants
ExposureMask:   dq 32768
KeyPressMask:   dq 1
KeyReleaseMask: dq 2

Expose:         dd 12
KeyPress:       dd 2
KeyRelease:		dd 3

XK_Escape:      dd 65307
XK_Up:          dd 65362
XK_Down:        dd 65364
XK_Z:           dd 'z'
XK_S:           dd 's'
key1:           dd 38
key2:           dd 233
key3:           dd 34

; Color name
black:		db "black", 0
white:      db "white", 0
