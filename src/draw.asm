set_color:
    mov rdi, [display]
    mov rsi, [gc_def]
    mov rdx, [xcols + eax * 8]
    sub rsp, 8
    call XSetForeground
    add rsp, 8
    ret

draw_game:
    xor eax, eax
    call set_color
    mov rdi, [display]
    mov rsi, [win]
    sub rax, 8
    call XClearWindow
    add rax, 8
    .draw_scoreP1:
        mov eax, [j1.score]
        mov dword [score_d], eax
        add dword [score_d], '0'
        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, 128
        mov r8, 15
        mov r9, score_d
        mov rax, 1
        push rax
        call XDrawString
        pop rax
    .draw_scoreP2:
        mov eax, [j2.score]
        mov dword [score_d], eax
        add dword [score_d], '0'
        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, 384
        mov r9, score_d
        mov rax, 1
        push rax
        call XDrawString
        pop rax
    .draw_pad_1:
        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, [j1.x_pad]
        mov r8, [j1.y_pad]
        mov r9, [pad_width]
        mov rax, [pad_height]
        push rax
        call XFillRectangle
        pop rax
    .draw_pad_2:
        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, [j2.x_pad]
        mov r8, [j2.y_pad]
        mov r9, [pad_width]
        mov rax, [pad_height]
        push rax
        call XFillRectangle
        pop rax
    .draw_bal:
        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, [ball.x]
        mov r8, [ball.y]
        mov r9, [ball.size]
        mov rax, [ball.size]
        push rax
        call XFillRectangle
        pop rax
    .draw_mid:
        xor rbx, rbx
        mov ebx, 2

        sub rsp, 8
        mov rax, 12
        push rax
        .draw_mid_loop:
            mov rdi, [display]
            mov rsi, [win]
            mov rdx, [gc_def]
            mov rcx, 252
            mov r8, rbx 
            mov r9, 1
            call XFillRectangle

            add ebx, 16
            cmp ebx, [height]
            jl .draw_mid_loop
        pop rax
        add rsp, 8
    ret
    
draw_menu:
    .j1Win:
        mov ecx, [j1.score]
        cmp ecx, 10
        jne .j2Win
        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, 230
        mov r8, 80
        mov r9, p1Win
        mov rax, winLen
        push rax
        call XDrawString
        pop rax
        jmp .menu
    .j2Win:
        mov ecx, [j2.score]
        cmp ecx, 10
        jne .null
        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, 230
        mov r8, 80
        mov r9, p2Win
        mov rax, winLen
        push rax
        call XDrawString
        pop rax
        jmp .menu
    .null:
        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, 230
        mov r8, 80
        mov r9, null
        mov rax, nullLen
        push rax
        call XDrawString
        pop rax
        jmp .menu

    .menu:
        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, 200
        mov r8, 130
        mov r9, fitstLine
        mov rax, len1
        push rax
        call XDrawString
        pop rax

        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, 200
        mov r8, 150
        mov r9, secondLine
        mov rax, len2
        push rax
        call XDrawString
        pop rax

        mov rdi, [display]
        mov rsi, [win]
        mov rdx, [gc_def]
        mov rcx, 200
        mov r8, 170
        mov r9, thirdLine
        mov rax, len3
        push rax
        call XDrawString
        pop rax
    ret