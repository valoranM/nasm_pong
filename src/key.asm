keyControl:    
    mov eax, [moove]
    .check_j2_up:
        cmp ecx, [XK_Up]
        jne .check_j2_down      ;== Up      P2 Up
        sub dword [j2.moove], eax
        jmp .end
    .check_j2_down:
        cmp ecx, [XK_Down]
        jne .check_j1_up         ;== Down    P2 Down
        add dword [j2.moove], eax
        jmp .end
    .check_j1_up:
        cmp ecx, [XK_Z]
        jne .check_j1_down      ;== 'z'     P1 Up
        sub dword [j1.moove], eax
        jmp .end
    .check_j1_down:
        cmp ecx, [XK_S]
        jne .end                ;== 'S      P1 down
        add dword [j1.moove], eax
        jmp .end
    .end:
        ret

inputControl:
    mov rdi, [display]
    mov rsi, [win]
    mov rdx, [KeyReleaseMask]
    or rdx, [KeyPressMask]
    mov rcx, xevent
    call XCheckWindowEvent

    cmp rax, 1
    jne .endProsEvent
    .keyEvent:
        mov dword [moove], 0
        .keyTypeCeck:
            ;if (xevent.type == KeyPress)
            mov ecx, [xevent + 0]
            cmp ecx, [KeyPress]
            jne .KeyReleaseCheck    ;!= KeyPress
            mov eax, [pad_speed]
            mov [moove], eax
            jmp .endKeyTypeCeck

        .KeyReleaseCheck:
            ;if (xevent.type == KeyRelease)
            cmp ecx, [KeyRelease]
            jne inputControl    ;!= KeyRelease
            mov eax, [pad_speed]
            neg eax
            mov [moove], eax

        .endKeyTypeCeck:
            mov rdi, [display]
            mov rsi, [xevent + 84]
            mov rdx, 0
            mov rcx, 0
            call XkbKeycodeToKeysym
            mov [keysym], rax
            mov ecx, [keysym]

            ; switch (keysym)
            cmp ecx, [XK_Escape]
            je .close        ;== XK_Escape

            call keyControl
            jmp inputControl

    .endProsEvent:
        xor rax, rax
        ret

    .close:
        mov rax, 1
        ret