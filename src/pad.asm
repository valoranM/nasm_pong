moovePad:
    mov ecx, [pad_speed]
    .robot:
        cmp dword [nbPlayer], 1
        jne .checkP2Up
        mov ebx, [ball.y]
        sub ebx, 28
        .robotUp:
            cmp ebx, 0
            jg .robotDown
            mov ebx, 0
            jmp .robotChangePos
        .robotDown:
            cmp ebx, [height]
            jl .robotChangePos
            mov ebx, [height]
            jmp .robotChangePos
        .robotChangePos:
            mov dword [j2.y_pad], ebx
    .checkP2Up:
        neg ecx
        cmp dword [j2.moove], ecx
        jne .checkP2Down      ;== Up      P2 Up
        cmp dword [j2.y_pad], 0
        jle .checkJ1Up                ;if j2.y <= 0
        mov eax, dword [j2.y_pad]        
        add eax, [j2.moove]
        mov dword [j2.y_pad], eax
        jmp .checkJ1Up
    .checkP2Down:
        neg ecx
        cmp dword [j2.moove], ecx
        jne .checkJ1Up         ;== Down    P2 Down
        mov eax, dword [j2.y_pad]
        add eax, [pad_height]
        cmp eax, [height]
        jge .checkJ1Up                ;if j2.y + pad_height >= height
        mov eax, dword [j2.y_pad]
        add eax, [j2.moove]
        mov dword [j2.y_pad], eax
        jmp .checkJ1Up
    .checkJ1Up:
        neg ecx
        cmp dword [j1.moove], ecx
        jne .checkJ1Down      ;== 'z'     P1 Up
        cmp dword [j1.y_pad], 0
        jle .end                ;if j1.y <= 0
        mov eax, dword [j1.y_pad]
        add eax, [j1.moove]
        mov dword [j1.y_pad], eax
        jmp .end
    .checkJ1Down:
        neg ecx
        cmp dword [j1.moove], ecx
        jne .end                ;== 'S      P1 down
        mov eax, dword [j1.y_pad]
        add eax, [pad_height]
        cmp eax, [height]
        jge .end                ;if j1.y + pad_height >= height
        mov eax, dword [j1.y_pad]
        add eax, [j1.moove]
        mov dword [j1.y_pad], eax
        jmp .end
    .end:
        ret