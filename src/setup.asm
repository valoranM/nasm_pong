alloc_color:
    push rbp
    mov rbp, rsp

    mov rdi, [display]
    mov rsi, [screen]
    sub rsp, 8
    call XDefaultColormap
    add rsp, 8
    mov [colormap], rax

    mov rdi, [display]
    mov rsi, [colormap]
    mov rdx, white
    mov rcx, xcols
    add rcx, xcol_struct.white
    mov r8, xcols
    add r8, xcol_struct.white
    sub rsp, 8
    call XAllocNamedColor
    add rsp, 8

    mov rdi, [display]
    mov rsi, [colormap]
    mov rdx, black
    mov rcx, xcols
    add rcx, xcol_struct.black
    mov r8, xcols
    add r8, xcol_struct.black
    sub rsp, 8
    call XAllocNamedColor
    add rsp, 8

    pop rbp
    ret

setup_xwindow:
    push rbp
    mov rbp, rsp

    ;Display* XOpenDisplay(NULL);
    mov rdi, 0
    sub rsp, 8
    call XOpenDisplay
    add rsp, 8
    mov [display], rax

    ;int XDefaultScreen(display);
    mov rdi, [display]
    sub rsp, 8
    call XDefaultScreen
    add rsp, 8
    mov [screen], eax

    mov rdi, [display]
    sub rsp, 8
    call XAutoRepeatOff
    add rsp, 8

    ;Window XDefaultRootWindow(display);
    mov rdi, [display]
    sub rsp, 8
    call XDefaultRootWindow
    add rsp, 8
    mov [parent], rax;

    ;uint XBlackPixel(display, screen);
    mov rdi, [display]
    mov rsi, [screen]
    sub rsp, 8
    call XBlackPixel
    add rsp, 8
    mov [gc_black], eax

    ;uint XWhitePixel(display, screen);
    mov rdi, [display]
    mov rsi, [screen]
    sub rsp, 8
    call XWhitePixel
    add rsp, 8
    mov [gc_white], eax

    ;Window XCreateSimpleWindow(display, parent, x, y, width, height, border_width, border, background);
    mov rdi, [display]
    mov rsi, [parent]
    mov rdx, 10
    mov rcx, 10

    mov r8d, [width]
    mov r9d, [height]

    mov rax, 1
    push rax
    mov eax, [gc_black]
    push rax
    push rax

    call XCreateSimpleWindow
    mov [win], rax
    add rsp, 24

    ;XSelectInput(display, win, ExposureMask | KeyPressMask);
    mov rdi, [display]
    mov rsi, [win]
    mov rdx, [KeyPressMask]
    or rdx, [KeyReleaseMask]
    sub rsp, 8
    call XSelectInput
    add rsp, 8

    ;GC DefaultGC(display, screen);
    mov rdi, [display]
    mov rsi, [screen]
    sub rsp, 8
    call XDefaultGC
    add rsp, 8
    mov [gc_def], rax

    call alloc_color

    ;XMapWindow(display, win);
    mov rdi,[display]
    mov rsi,[win]
    sub rsp, 8
    call XMapWindow
    add rsp, 8

	pop	rbp
    ret

setup_data:
    mov dword [j1.x_pad], 10
    mov dword [j1.y_pad], 100
    mov dword [j1.score], 0
    mov dword [j1.moove], 0
    mov dword [j2.x_pad], 492
    mov dword [j2.y_pad], 100
    mov dword [j2.moove], 0
    mov dword [j2.score], 0
    mov dword [ball.x], 251
    mov dword [ball.y], 123
    mov dword [ball.x_dir], 1
    mov dword [ball.y_dir], -1
    mov dword [ball.x_speed], 1
    mov dword [ball.y_speed], 1
    ret